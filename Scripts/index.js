(() => {
    "use strict";

    const images = [
        '../Images/0.png',
        '../Images/1.jpg',
        '../Images/2.jpg',
        '../Images/3.jpg',
        '../Images/4.jpg',
        '../Images/5.jpg',
        '../Images/6.png',
        '../Images/7.jpg',
        '../Images/8.jpg',
        '../Images/9.png',
        '../Images/a.png',
        '../Images/b.png',
        '../Images/c.png',
        '../Images/d.png',
        '../Images/e.png',
        '../Images/f.png',
        '../Images/g.png',
        '../Images/h.png',
        '../Images/i.png',
        '../Images/j.png',
        '../Images/k.png',
        '../Images/l.png',
        '../Images/m.png',
        '../Images/n.png',
        '../Images/o.png',
        '../Images/p.png',
        '../Images/q.png',
        '../Images/r.png'
    ];

    document.querySelector("html").setAttribute("style", `--image: url('${images[Math.floor(Math.random()*images.length)]}')`)
})();